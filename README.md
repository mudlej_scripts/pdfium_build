* On an Ubuntu system
* git clone https://github.com/bblanchon/pdfium-binaries.git
* cd pdfium-binaries/
* Edit ./build.sh
* Define the following variables under 'set -ex'

```bash
PDFium_TARGET_CPU=arm           # change this to build for diffrent CPU
PDFium_TARGET_OS=android
PDFium_TARGET_LIBC=default
PDFium_BRANCH=main
PDFium_ENABLE_V8=false
PDFium_IS_DEBUG=false
```

* run ./build.sh
* The build file is available at ./build/libpdfium.so
* Change its name to libmodpdfium.so and copy it to 'jni/lib/CPU/'

* remove .git files at pdfium-binaries/:<br />
    $ find -name ".git" | xargs sudo rm -rf

* Change PDFium_TARGET_CPU to build for different CPUs<br />
    x86     ->  x86<br />
    x64     ->  x86_64<br />
    arm     ->  armeabi-v7a<br />
    arm64   ->  arm64-v8a<br />

